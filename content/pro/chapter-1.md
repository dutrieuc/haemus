---
date: 2017-04-09T10:58:08-04:00
description: ""
featured_image: "/images/Pope-Edouard-de-Beaumont-1844.jpg"
tags: ["press"]
title: "Press extract"
---

Mozart's music is free of all exaggeration, of all sharp breaks and
contradictions. The sun shines but does not blind, does not burn or
consume. Heaven arches over the earth, but it does not weigh it down, it
does not crush or devour it. Hence earth remains earth, with no need to
maintain itself in a titanic revolt against heaven. Granted, darkness,
chaos, death and hell do appear, but not for a moment are they allowed
to prevail. Knowing all, Mozart creates music from a mysterious center,
and so knows the limits to the right and the left, above and below. He
maintains moderation.

- *As quoted in 'Amadeus : Sin, Salvation and Salieri' in Saint Paul at the Movies : The Apostle's Dialogue with American Culture (1993) by Robert Jewett, p. 37, and Melting the Venusberg : A Feminist Theology of Music (2004) by Heidi Epstein, p.
72.*
