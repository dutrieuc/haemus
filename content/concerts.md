---
title: "Concerts"
featured_image: "/images/Victor_Hugo-Hunchback.jpg"
description: "The upcoming tour dates"
type: page
menu:
  main:
    weight: 30
---

This is an example of a custom shortcode that you can put right into
your content. You will need a service that provide the export of calendar
events as ics files. [Mobilizon](https://mobilizon.fr/) is a free
open-source tool that allows it. You could also find an instance of
Nextcloud or Radicale that does it.

{{< calendar-events ics="https://mobilizon.fr/@chatons/feed/ics" history="3">}}
