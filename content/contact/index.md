---
title: Contact
featured_image: ''
description: 
type: page
menu:
  main:
    weight: 100


---


You can contact us at mozart@example.org


# Contact
Or this is an example of a custom shortcode that you can put right into your content. You will need to add a form action to the shortcode to make it work. Check out [Formspree](https://formspree.io/) for a simple, free form service.

{{< form-contact action="https://example.com"  >}}
