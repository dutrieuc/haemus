---
title: "About Us"
description:
  "Lorem ipsum dolor sit amet consectetur adipiscing elit rutrum
  facilisi class auctor lectus, vel ornare fusce mollis duis sem velit
  vivamus ac nullam metus, venenatis semper phasellus conubia purus hac
  sociis senectus mi ligula varius. Morbi pharetra ad donec metus tortor
  arcu commodo viverra lacus, faucibus imperdiet augue tempus est
  feugiat a enim gravida quam, inceptos varius tellus bibendum at
  senectus erat consequat. Dictumst nascetur justo nullam cubilia
  sagittis libero ornare erat, vulputate tincidunt condimentum fames
  potenti ut faucibus nibh aliquet, dis lacus integer habitasse turpis
  odio et."
featured_image: "/images/Victor_Hugo-Hunchback.jpg"
menu:
  main:
    weight: 20
---

Lobortis faucibus condimentum massa per ultrices lectus laoreet egestas,
sem molestie eget ornare pharetra eleifend et, diam ridiculus arcu
suspendisse praesent cras posuere. Convallis quam montes quis libero
cras id etiam aliquet lobortis dictum imperdiet congue conubia vehicula
volutpat, porta erat praesent ligula dis sodales sollicitudin auctor
commodo nisi potenti turpis nunc non. Vivamus quam vulputate pretium
felis cubilia volutpat placerat, vestibulum vel blandit curae hendrerit
fames quisque, fringilla lobortis mauris sed class nulla.

{{< row >}}
{{< column >}}
{{< figure src="/images/mozart.jpg" title="Mozart _composer, piano_" >}}
{{< /column >}}

{{< column >}}
{{< figure src="/images/joseph-leutgeb.jpg" title="Joseph Leutbeg _horn_" >}}
{{< /column >}}
{{< /row >}}
