---
date: 1772-12-05T10:58:08-04:00
description: ""
featured_image: "/images/Interior_of_the_Teatro_Regio_Ducale_Milan_1742.jpg"
tags: ["scene"]
title: "Milan"
---

I have now about fourteen pieces to write, and then I shall have
finished. Indeed, the trio and the duet may be considered as four. I
cannot possibly write much, for I have no news, and in the next place I
scarcely know what I am writing, as all my thoughts are absorbed in my
opera, so there is some danger of my writing you a whole aria instead of
a letter. I have learned a new game here, called mercanti in fiera. As
soon as I come home we can play at it together. I have also learned a
new language from Frau von Taste, which is easy to speak, though
troublesome to write, but still useful. It is, I own, rather a little
childish, but will do capitally for Salzburg. My kind regards to pretty
Nandl and to the canary, for these two and yourself are the most
innocent creatures in our house. Fischietti will no doubt soon begin to
work at his opera buffa.

Addio!
